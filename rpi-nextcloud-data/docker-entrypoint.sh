#!/bin/bash
set -e

sudo sed -i "s@\"dbuser\".*,\$@\"dbuser\" => \"$NEXTCLOUD_DB_USER\",@g" $NEXTCLOUD_CONFIG_DIR/autoconfig.php && \
sudo sed -i "s@\"dbpass\".*,\$@\"dbpass\" => \"$NEXTCLOUD_DB_PASSWORD\",@g" $NEXTCLOUD_CONFIG_DIR/autoconfig.php && \

exec "$@"
