<?php
$AUTOCONFIG = array(
"directory" => "$NEXTCLOUD_DATA_DIR",
"dbtype" => "mysql",
"dbname" => "nextcloud",
"dbuser" => "$NEXTCLOUD_DB_USER",
"dbpass" => "$NEXTCLOUD_DB_PASSWORD",
"dbhost" => "mysql",
"dbtableprefix" => "nc_",
);
